

fetch("https://jsonplaceholder.typicode.com/todos")
  .then(res => res.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
    return titles;
  })


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

/*
Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
*/

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		userId: 1
	})

})
.then(res => res.json())
.then(json => console.log(json))



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title:"Updated To Do List Item",
		description:"To update my to do list with a different data structure.",
		status:"Pending",
		dateCompleted:"Pending",
		userId:1
		
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status:"Complete",
		dateCompleted:"30/03/2023",
		userId:1,
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
})

